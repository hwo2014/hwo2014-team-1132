from math import sin, sqrt, pi, cos

__author__ = 'ps'

#przyda sie przy obliczaniu kosztu zmiany pasa
def arc_length(func, a, b):
    points = []
    precision = 100
    for i in xrange(precision + 1):
        x = a + i * (b - a) / precision
        points.append((x, func(x)))
    length = 0
    for i in xrange(len(points) - 1):
        x_diff = points[i + 1][0] - points[i][0]
        y_diff = points[i + 1][1] - points[i][1]
        length += sqrt(x_diff ** 2 + y_diff ** 2)
    return length

arc_length(lambda x: 10 * cos(x * pi / 100.0), 0, 10)