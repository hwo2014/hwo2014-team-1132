import json
import socket
import sys
from track import Track


class CarPosition(object):
    def __init__(self, piece, s_lane, e_lane, distance, angle):
        self.piece = piece
        self.s_lane = s_lane
        self.e_lane = e_lane
        self.distance = distance
        self.angle = angle


def calculate_distance(a, b):
    if a.piece == b.piece:
        return b.distance - a.distance
    else:
        if a.s_lane != a.e_lane:
            pass


class NoobBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.track = None
        self.position = None

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
        # return self.msg("joinRace",
        #                 {"botId": {"name": self.name, "key": self.key}, "trackName": "germany", "carCount": 1})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def switch_lane(self, whichLane):
        self.msg("switchLane", whichLane)

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        idx = data[0]['piecePosition']['pieceIndex']
        s_lane = data[0]['piecePosition']['lane']['startLaneIndex']
        e_lane = data[0]['piecePosition']['lane']['startLaneIndex']
        distance = data[0]['piecePosition']['inPieceDistance']
        angle = data[0]['angle']
        new_position = CarPosition(self.track.pieces[idx], self.track.lanes[s_lane], self.track.lanes[e_lane], distance, angle)
        if self.position is not None:
            #TODO: obliczanie przebytej drogi new_position - self.position w ostatnim "ticku"
            pass
        length = self.track.pieces[idx].get_length(self.track.lanes[s_lane])
        if distance > length:
            print("%f %f" % (distance, length))
            print(self.track.pieces[idx])
        if s_lane == e_lane:
            sficz = self.track.track_graph.sficz(idx, s_lane)
            if sficz is not None:
                # print(sficz)
                self.msg("switchLane", sficz)
        # print(idx)
        self.position = new_position
        self.throttle(0.65)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_init(self, data):
        self.track = Track(data)

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_init,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
