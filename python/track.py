from math import pi


class TrackPiece(object):
    def __init__(self, next_piece, switch=False):
        self.next_piece = next_piece
        self.switch = switch


class StraightPiece(TrackPiece):
    def __init__(self, length, next_piece, switch):
        super(StraightPiece, self).__init__(next_piece, switch)
        self.length = length

    def get_length(self, lane=None):
        return self.length

    def __str__(self):
        return "Straight length: %f switch: %r next: %d" % (self.length, self.switch, self.next_piece)


class BendPiece(TrackPiece):
    def __init__(self, radius, angle, next_piece, switch):
        super(BendPiece, self).__init__(next_piece, switch)
        self.radius = radius
        self.angle = angle

    def get_length(self, lane):
        if self.angle > 0:
            r = self.radius - lane.distance
        else:
            r = self.radius + lane.distance
        return abs(self.angle / 360) * 2 * pi * r

    def __str__(self):
        return "Bend angle: %f radius: %f switch: %r next: %d" % (self.angle, self.radius, self.switch, self.next_piece)


class Lane(object):
    def __init__(self, distance, index):
        self.distance = distance
        self.index = index


class TrackGraph(object):
    def __init__(self, e, v):
        for _ in e:
            print(_)
        print(v)
        self.e = e
        self.v = v[-1:] + v[:-1]

    # self.e => suma dlugosci kawalkow toru na poszczegolnych pasach na kolejnych odcinkach pomiedzy miejscami gdzie mozna zmienic pas
    # self.e[x] => pas x
    # self.v => v[x] indeksy kawalkow torow ktore leza przed x-tym i po (x-1)-tym miejscem gdzie mozna zmienic pas
    # do dupy! 2 tory jeszcze moze spoko
    def sficz(self, piece, lane):
        idx = None
        for i in xrange(len(self.v)):
            if piece in self.v[i]:
                idx = i

        #nie uwzglednia kosztu zmiany pasa
        if idx is not None:
            if lane == 0 and self.e[1][idx] < self.e[0][idx]:
                return 'Right'
            elif lane == 1 and self.e[0][idx] < self.e[1][idx]:
                return 'Left'
            else:
                return None


class Track(object):
    def __init__(self, data):
        self.pieces = []
        pieces_data = data['race']['track']['pieces']
        self.init_pieces(pieces_data)
        self.lanes = []
        lanes_data = data['race']['track']['lanes']
        self.init_lanes(lanes_data)
        self.track_graph = self.make_graph()
        # for p in self.pieces:
        #     print(p)
        for l in self.lanes:
            print(l.distance)

    def init_pieces(self, data):
        for i in xrange(len(data)):
            p = data[i]
            next = i + 1
            if next == len(data):
                next = 0
            switch = False
            if 'switch' in p:
                switch = p['switch']
            if 'length' in p:
                self.pieces.append(StraightPiece(p['length'], next, switch))
            else:
                self.pieces.append(BendPiece(p['radius'], p['angle'], next, switch))

    def init_lanes(self, data):
        for lane in data:
            self.lanes.append(Lane(lane['distanceFromCenter'], lane['index']))

    def find_first_switch(self, start=0):
        switch_test = False
        switch_idx = start - 1
        while not switch_test:
            switch_idx += 1
            switch_test = self.pieces[switch_idx].switch
        return switch_idx

    def make_graph(self):
        switch_idx = self.find_first_switch()
        i = switch_idx + 1
        lanes_count = len(self.lanes)
        len_from_last_switch = [0] * lanes_count
        e = [[] for _ in xrange(lanes_count)]
        v = []
        indexes = []
        complete = False
        while not complete:
            # check if lap is completed
            if i == switch_idx:
                complete = True
                # check if current piece is a switch
            if self.pieces[i].switch:
                # push accumulated length from last switch
                for j in xrange(lanes_count):
                    e[j].append(len_from_last_switch[j])
                len_from_last_switch = [0] * lanes_count
                v.append(indexes)
                indexes = []
            else:
                indexes.append(i)
                # current piece length add to accumulated length
                for j in xrange(lanes_count):
                    len_from_last_switch[j] += self.pieces[i].get_length(self.lanes[j])
            i = self.pieces[i].next_piece
        return TrackGraph(e, v)
